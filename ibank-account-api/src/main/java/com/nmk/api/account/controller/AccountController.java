package com.nmk.api.account.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nmk.api.account.service.AccountService;
import com.nmk.ibank.api.controller.IBankBaseRestController;
import com.nmk.ibank.api.util.AccountServiceUtil;
import com.nmk.ibank.model.Account;
import com.nmk.ibank.model.list.AccountList;
import com.nmk.ibank.service.entity.IbankAccount;

@RestController
@RequestMapping("/api/v1/account")
public class AccountController  extends IBankBaseRestController{
	
	private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

	
	@Autowired
	AccountService accountService;

/*	@RequestMapping("/")
	String hello() {
		logger.debug("Debug message");
		logger.info("Info message");
		logger.warn("Warn message");
		logger.error("Error message");
		return "Done";
	}*/

	@GetMapping(value = "", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public AccountList getAllDetails() {
		logger.debug("Start : AccountController .getAllDetails()");
		AccountList ecommAccountList = accountService.findAll();

		logger.debug("End : AccountController .getAllDetails()");
		return ecommAccountList;
	}

	@GetMapping(value = "/{idx}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public Account getAccountByIdx(@PathVariable(value = "idx") Integer accountidx) {

		logger.debug("Start : AccountController .getAccountByIdx()");
		IbankAccount ibankAccount = accountService.findAccountByIdx(accountidx);

		logger.debug("End : AccountController .getAccountByIdx()");
		return AccountServiceUtil.convertibankAccountObjectToAccount(ibankAccount);
	}

	@PostMapping(value = "", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public Account createAccount(@Valid @RequestBody Account account) {
		logger.debug("Start : CardTypeController .createCardType()");
		IbankAccount ibankAccount = AccountServiceUtil.convertAccountToibankAccount(account);

		IbankAccount ibankAccount1 = accountService.createAccount(ibankAccount);
		logger.debug("End : CardTypeController .createCardType()");

		return AccountServiceUtil.convertibankAccountObjectToAccount(ibankAccount1);
	}
	
	@DeleteMapping(value = "/{idx}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> deleteAccount(@PathVariable(value = "idx") Integer accountidx) {
		logger.debug("Start : CardTypeController .deleteCardType()");
		accountService.deleteAccount(accountidx);

		logger.debug("End : CardTypeController .deleteCardType()");
		return ResponseEntity.ok().build();
	}

	@PutMapping(value="/{idx}",produces= {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
	public Account updateBrand(@PathVariable(value="idx") Integer accountidx , @Valid @RequestBody Account account) {
		

		logger.debug("Start : CardTypeController .updateCardType()");
		IbankAccount IbankAccount = accountService.updateAccount(account, accountidx);

		logger.debug("End : CardTypeController .updateCardType()");
		return AccountServiceUtil.convertibankAccountObjectToAccount(IbankAccount);

				
	}
	
	

}
