package com.nmk.api.account.service;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nmk.api.account.repository.AccountRepository;
import com.nmk.ibank.api.exception.IBankHTTP404Exception;
import com.nmk.ibank.api.util.AccountServiceUtil;
import com.nmk.ibank.model.Account;
import com.nmk.ibank.model.list.AccountList;
import com.nmk.ibank.service.entity.IbankAccount;

@Service
public class AccountServiceImpl implements AccountService {
	
	private static final Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);
	
	@Autowired
	AccountRepository accountRepository;

	

	@Override
	public IbankAccount createAccount(IbankAccount ibankAccount) {

		logger.debug("Start : AccountServiceImpl.createAccount()");
		logger.debug("End : AccountServiceImpl.createAccount()");
		return accountRepository.save(ibankAccount);

	}

	@Override
	public IbankAccount findAccountByIdx(int accountIdx) {

		logger.debug("Start : AccountServiceImpl.findAccountByIdx()");
		IbankAccount ibankAccount = accountRepository.findById(accountIdx)
				.orElseThrow(() -> new IBankHTTP404Exception("idx-" + accountIdx + " is not exist"));

		logger.debug("End : AccountServiceImpl.findAccountByIdx()");
		return ibankAccount;
	}

	@Override
	public IbankAccount updateAccount(Account account, Integer accountIdx) {
		logger.debug("Start : AccountServiceImpl.updateAccount()");
		IbankAccount ibankAccount = accountRepository.findById(accountIdx)
				.orElseThrow(() -> new IBankHTTP404Exception("idx-" + accountIdx + " is not exist"));
		
		
		ibankAccount.setName(account.getName());
		ibankAccount.setDescription(account.getDescription());
		ibankAccount.setOpenDate(account.getOpenDate());
		ibankAccount.setClosedDate(account.getClosedDate());
		ibankAccount.setLocationIdx(account.getLocationIdx());
		ibankAccount.setUserUId(account.getUserUId());
		ibankAccount.setModifiedBy(account.getModifiedBy());
		ibankAccount.setModifiedDate(new Date());
		
		IbankAccount updatedAccount = accountRepository.save(ibankAccount);

		logger.debug("End : AccountServiceImpl.updateAccount()");
		return updatedAccount;

	}

	@Override
	public IbankAccount deleteAccount(int accountIdx) {

		logger.debug("Start : AccountServiceImpl.deleteAccount()");
		IbankAccount ibankAccount = accountRepository.findById(accountIdx)
				.orElseThrow(() -> new IBankHTTP404Exception("idx-" + accountIdx + " is not exist"));

		logger.debug("End : AccountServiceImpl.deleteAccount()");
		accountRepository.delete(ibankAccount);
		return ibankAccount;
	}

	@Override
	public AccountList findAll() {
		logger.debug("Start : AccountServiceImpl.findAll()");
		List<IbankAccount> ibankAccountList = accountRepository.findAll();
		List<Account> list = AccountServiceUtil.convertibankAccountListToAccount(ibankAccountList);
		AccountList accountList = new AccountList();
		accountList.setData(list);

		logger.debug("End : AccountServiceImpl.findAll()");
		return accountList;
	}

}
