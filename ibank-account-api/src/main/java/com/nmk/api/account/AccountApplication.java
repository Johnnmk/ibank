package com.nmk.api.account;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import com.nmk.ibank.api.log.LoggingFilter;

@SpringBootApplication
@EntityScan("com.nmk.ibank.service.entity")
@EnableJpaAuditing
public class AccountApplication {

	public static void main(String args[]) {
		SpringApplication.run(AccountApplication.class, args);
	}
	@Bean
	public FilterRegistrationBean<LoggingFilter> loggingFilter() {
		FilterRegistrationBean<LoggingFilter> registrationBean = new FilterRegistrationBean<>();
		registrationBean.setFilter(new LoggingFilter());
		registrationBean.addUrlPatterns("/api/*");
		return registrationBean;
	}	
}
