package com.nmk.api.account.service;

import com.nmk.ibank.model.Account;
import com.nmk.ibank.model.list.AccountList;
import com.nmk.ibank.service.entity.IbankAccount;

public interface AccountService {


	public IbankAccount createAccount(IbankAccount ibankAccount);

	public IbankAccount findAccountByIdx(int accountIdx);

	public IbankAccount updateAccount(Account account, Integer accountIdx);

	public IbankAccount deleteAccount(int accountIdx);

	public AccountList findAll();
}
